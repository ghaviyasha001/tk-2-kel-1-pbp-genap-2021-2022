# TK 2 Kel 1 PBP Genap 2021-2022
## Anggota Kelompok 1:
- Khairinka Rania Lizadhi 2006597254
- Elang Permana 2006520405
- Satriaji Najha Darmawan 2006596226
- Adam Ghaviyasha 2006482584
- Patrick Alexander 1906398710
- Ridho Mulia 2006597866
- Fauzan Daffa Raditya 2106752363
- Hadist Fadhillah 2006463490

## _Web app_ olahraga
Pada TK 2, kelompok kami akan mengadaptasi fitur-fitur dari _web app_ kami yaitu:
- Kumpulan berita olahraga
- _Scoreboard_ pertandingan
- _Schedule_ pertandingan
- Forum olahraga
- _Game_/trivia - Adam Ghaviyasha
- Profil atlet - Fauzan Daffa Raditya
- _Ranking_ atlet - Satriaji Najha Darmawan

_Mobile app_ ini akan memberikan manfaat melalui hiburan seputar olahraga serta game yang disediakan. Para pengguna yang memiliki rasa ingin tahu yang tinggi juga dapat mengenal para atlet favoritnya lebih dalam lagi. Selain itu, pengguna juga mendapatkan wadah untuk saling berinteraksi.

## User Persona
_Mobile app_ kami ditujukan untuk tiga tipe persona, yaitu:
1. User yang ingin _up to date_ dengan berita/skor olahraga yang disukainya
2. User yang ingin berinteraksi dengan sesama penggemar olahraga
3. User yang ingin lebih tau trivia seputar olahraga dan atlet-atletnya

Pemetaan dari tiap _user persona_ ini terhadap setiap fitur kurang lebih adalah sebagai berikut:

**User Persona #1**
- Kumpulan berita olahraga
- _Scoreboard_ pertandingan
- _Schedule_ pertandingan

**User Persona #2**
- Forum olahraga

**User Persona #3**
- _Game_/trivia
- Profil atlet
- _Ranking_ atlet
