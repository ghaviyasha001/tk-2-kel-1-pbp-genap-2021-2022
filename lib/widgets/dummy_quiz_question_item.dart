import 'package:flutter/material.dart';

import '../screens/quiz_screen.dart';

class DummyQuizQuestionItem extends StatelessWidget {
  final int quiz_id;

  final String name;
  final String desc;
  
  final String question0;
  final String question1;
  final String question2;
  final String question3;
  final String question4;
  
  final Color color;

  DummyQuizQuestionItem(this.quiz_id, this.name, this.desc, this.question0, this.question1, this.question2, this.question3, this.question4, this.color);

  void selectQuiz(BuildContext ctx) {
    QuizTakeScreen.name = this.name;
    QuizTakeScreen.questions = [this.question0, this.question1, this.question2, this.question3, this.question4];

    Navigator.of(ctx).pushNamed(
      QuizTakeScreen.routeName
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectQuiz(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Text(name, style: Theme.of(context).textTheme.headline6),
            Text(desc, style: Theme.of(context).textTheme.bodyText1),
          ]
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}