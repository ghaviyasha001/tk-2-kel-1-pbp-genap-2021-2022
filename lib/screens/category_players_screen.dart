import 'package:flutter/material.dart';

import '../widgets/player_item.dart';
import '../models/player.dart';

class CategoryPlayersScreen extends StatefulWidget {
  static const routeName = '/category-players';

  final List<Player> availablePlayers;

  CategoryPlayersScreen(this.availablePlayers);

  @override
  _CategoryPlayersScreenState createState() => _CategoryPlayersScreenState();
}

class _CategoryPlayersScreenState extends State<CategoryPlayersScreen> {
  String categoryTitle;
  List<Player> displayedPlayers;
  var _loadedInitData = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      displayedPlayers = widget.availablePlayers.where((player) {
        return player.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return PlayerItem(
            id: displayedPlayers[index].id,
            title: displayedPlayers[index].title,
            imageUrl: displayedPlayers[index].imageUrl,
          );
        },
        itemCount: displayedPlayers.length,
      ),
    );
  }
}
