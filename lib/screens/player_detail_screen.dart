import 'package:flutter/material.dart';

import '../dummy_data.dart';

class PlayerDetailScreen extends StatelessWidget {
  static const routeName = '/player-detail';

  final Function toggleFavorite;
  final Function isFavorite;

  PlayerDetailScreen(this.toggleFavorite, this.isFavorite);

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final playerId = ModalRoute.of(context).settings.arguments as String;
    final selectedPlayer = DUMMY_PLAYERS.firstWhere((player) => player.id == playerId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedPlayer.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                selectedPlayer.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildSectionTitle(context, 'Description'),
            buildContainer(
              Text('${selectedPlayer.description}'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
           isFavorite(playerId) ? Icons.star : Icons.star_border,
        ),
        onPressed: () => toggleFavorite(playerId),
      ),
    );
  }
}
