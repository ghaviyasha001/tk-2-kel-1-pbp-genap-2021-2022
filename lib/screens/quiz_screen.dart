import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './form_screen.dart';
import './categories_screen.dart';

import '../widgets/main_drawer.dart';
import '../widgets/dummy_quiz_question_item.dart';

import '../models/quiz_question.dart';
import '../models/quiz_answer.dart';
import '../models/quiz_submission.dart';
import '../env.sample.dart';

import '../dummy_data.dart';

class QuizEnterScreen extends StatelessWidget {
  //
  static const routeName = '/quiz-enter-screen';

  //
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      appBar: AppBar(
        title: Text("Sports Trivia Quiz"),
      ),

      //
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            //
            Text("Feel like a sports wiz?\nRaring to prove your worth as a trivia master?\nTake our quiz! And maybe like the great King Arthur, you'll pull the sword out of the preverbial stone."),

            //
            ElevatedButton(
              child: Text('Take a Quiz'),
              onPressed: () {
                Navigator.push(
                  context, MaterialPageRoute(builder: (context) => QuizSelectScreen()),
                );
              },
            ),
          ]
        )
      ),

      //
      drawer: MainDrawer(),
    );
  }
}

class QuizSelectScreen extends StatefulWidget {
  //
  static const routeName = '/quiz-select-screen';

  @override
  QuizSelectState createState() => QuizSelectState();
}

class QuizSelectState extends State<QuizSelectScreen> {
  //
  final quizQuestionListKey = GlobalKey<QuizSelectState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      key: quizQuestionListKey,

      //
      appBar: AppBar(
        title: Text("Select a quiz!"),
      ),

      //
      body: GridView(
        padding: const EdgeInsets.all(25),
        children: DUMMY_QUIZ_QUESTIONS
        .map(
          (catData) => DummyQuizQuestionItem(
            catData.quiz_id,

            catData.name,
            catData.desc,

            catData.question0,
            catData.question1,
            catData.question2,
            catData.question3,
            catData.question4,

            Colors.red,
          ),
        )
        .toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
      ),

      //
      drawer: MainDrawer(),
    );
  }
}

  /*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      key: quizQuestionListKey,

      //
      appBar: AppBar(
        title: Text("Select a quiz"),
      ),

      //
      body: Center(
        child: FutureBuilder<List<QuizQuestion>>(
          future: quizQuestions,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) {
              print(snapshot);
              return CircularProgressIndicator();
            }

            // Render employee lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    title: Text(
                      data.name,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                );
              },
            );
          },
        )
      ),

      //
      drawer: MainDrawer(),
    );
  }
}
*/

class QuizTakeScreen extends StatelessWidget {
  //
  static const routeName = '/quiz-take-screen';

  static String name = "";
  static List<String> questions = [];

  //
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      appBar: AppBar(
        title: Text(name)
      ),

      //
      body: Center(
        child: ListView.builder(
          itemCount: questions.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: ListTile(
                title: Text(
                  questions[index],
                  style: TextStyle(fontSize: 12),
                ),
              ),
            );
          },
        ),
      ),

      //
      drawer: MainDrawer(),
    );
  }
}