import 'package:flutter/foundation.dart';

class QuizAnswer {
  final String answer_0;
  final String answer_1;
  final String answer_2;
  final String answer_3;
  final String answer_4;

  final int quiz_id;

  const QuizAnswer({
    @required this.answer_0,
    @required this.answer_1,
    @required this.answer_2,
    @required this.answer_3,
    @required this.answer_4,

    @required this.quiz_id,
  });
}
