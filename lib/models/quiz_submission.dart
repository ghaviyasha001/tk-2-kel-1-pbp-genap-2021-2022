import 'package:flutter/foundation.dart';

class QuizSubmission {
  final String submit_0;
  final String submit_1;
  final String submit_2;
  final String submit_3;
  final String submit_4;

  final String name;
  final String title;

  final int quiz_id;

  const QuizSubmission({
    @required this.submit_0,
    @required this.submit_1,
    @required this.submit_2,
    @required this.submit_3,
    @required this.submit_4,

    @required this.name,
    @required this.title,

    @required this.quiz_id,
  });
}
