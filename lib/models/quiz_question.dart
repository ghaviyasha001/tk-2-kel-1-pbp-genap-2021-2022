import 'package:flutter/foundation.dart';

class QuizQuestion {
  final String question_0;
  final String question_1;
  final String question_2;
  final String question_3;
  final String question_4;

  final String name;
  final String desc;

  final int quiz_id;

  QuizQuestion({this.question_0, this.question_1, this.question_2, this.question_3, this.question_4, this.name, this.desc, this.quiz_id});

  factory QuizQuestion.fromJson(Map<String, dynamic> json) {
    return QuizQuestion(
      question_0: json['question_0'],
      question_1: json['question_1'],
      question_2: json['question_2'],
      question_3: json['question_3'],
      question_4: json['question_4'],
      
      name: json['name'],
      desc: json['desc'],
      
      quiz_id: json['quiz_id'],
    );
  }

  Map<String, dynamic> toJson() => {
    'question_0' : question_0,
    'question_1' : question_1,
    'question_2' : question_2,
    'question_3' : question_3,
    'question_4' : question_4,

    'name' : name,
    'desc' : desc,

    'quiz_id' : quiz_id,
  };
}
