import 'package:flutter/foundation.dart';

class DummyQuizQuestion {
  final int quiz_id;

  final String name;
  final String desc;

  final String question0;
  final String question1;
  final String question2;
  final String question3;
  final String question4;

  const DummyQuizQuestion({
    @required this.quiz_id,

    @required this.name,
    @required this.desc,

    @required this.question0,
    @required this.question1,
    @required this.question2,
    @required this.question3,
    @required this.question4,
  });
}
