import 'package:flutter/material.dart';

import './models/category.dart';
import './models/player.dart';
import './models/dummy_quiz_question.dart';

const DUMMY_QUIZ_QUESTIONS = const [
  DummyQuizQuestion(
    quiz_id: 1,
    name: 'Sports Quiz #1',
    desc: 'The first iteration',
    question0: "1. What's the diameter of a basketball hoop in inches?\r\nA. 18 inches\r\nB. 17 inches\r\nC. 16 inches\r\nD. 15 inches\r\nE. 14 inches",
    question1: "2. The Olympics are held every how many years?\r\nA. 1 year \r\nB. 2 years\r\nC. 3 years \r\nD. 4 years\r\nE. 5 years",
    question2: "3. What sport is best known as the 'king of sports'?\r\nA. American football\r\nB. Basketball\r\nC. Cross-country\r\nD. Soccer\r\nE. Golf",
    question3: "4. What do you call it when a bowler makes three strikes in a row?\r\nA. Chicken dinner\r\nB. Turkey\r\nC. Hattrick\r\nD. Trigol\r\nE. Triple kill",
    question4: "5. What's the national sport of Canada?\r\nA. Hockey\r\nB. Cricket\r\nC. Lacrosse\r\nD. Snowboarding\r\nE. Elk hunting",
  ),
  DummyQuizQuestion(
    quiz_id: 2,
    name: 'Sports Quiz #2',
    desc: 'Electric boogaloo',
    question0: "1. How many dimples does an average golf ball have?\r\nA. 12\r\nB. 36\r\nC. 125\r\nD. 336\r\nE. 512",
    question1: "2. What country has competed the most times in the Summer Olympics yet hasn't won a gold medal?\r\nA. Indonesia\r\nB. Malaysia\r\nC. Thailand\r\nD. The Philippines\r\nE. Vietnam",
    question2: "3. The classic 1980 movie called Raging Bull is about which real-life boxer?\r\nA. Jake LaMotta\r\nB. Jack Ma\r\nC. Mike Tyson\r\nD. Muhammad Ali\r\nE. Muhammad Ridwan",
    question3: "4. How many medals did China win at the Beijing Olympics?\r\nA. 5\r\nB. 9\r\nC. 7\r\nD. 4\r\nE. 100",
    question4: "5. In the 1971 Olympics, Nadia Comaneci was the first gymnast to score a perfect score. What country was she representing?\r\nA. Rome\r\nB. Romania\r\nC. Lithuania\r\nD. Greece\r\nE. Turkmenistan",
  ),
];

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Forward',
    color: Colors.purple,
  ),
  Category(
    id: 'c2',
    title: 'Midfielder',
    color: Colors.red,
  ),
  Category(
    id: 'c3',
    title: 'Defender',
    color: Colors.orange,
  ),
  Category(
    id: 'c4',
    title: 'Goalkeeper',
    color: Colors.amber,
  ),
];

const DUMMY_PLAYERS = const [
  Player(
    id: 'm1',
    categories: [
      'c1',
      'c3',
    ],
    title: 'Cristiano Ronaldo',
    imageUrl:
        'https://assets.pikiran-rakyat.com/crop/151x0:1903x1204/x/photo/2022/05/14/1149393770.jpg',
    description: 'Cristiano Ronaldo dos Santos Aveiro adalah seorang pemain sepak bola profesional asal Portugal yang bermain sebagai penyerang untuk klub Liga Inggris, Manchester United, dan juga kapten untuk tim nasional Portugal.',
  ),
  Player(
    id: 'm2',
    categories: [
      'c1',
      'c2',
    ],
    title: 'Lionel Messi',
    imageUrl:
        'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltee41eef7e0a0e3f3/626b8172918f4e4e59121e78/GettyImages-1240053215.jpg',
    description: 'Lionel Andrés Messi juga dikenal sebagai Leo Messi, adalah seorang pemain sepak bola profesional asal Argentina yang bermain sebagai penyerang untuk klub Ligue 1 Paris Saint-Germain dan merupakan kapten tim nasional Argentina.',
  ),
  Player(
    id: 'm3',
    categories: [
      'c1',
      'c2',
      'c3',
    ],
    title: 'Neymar',
    imageUrl:
        'https://idsb.tmgrup.com.tr/ly/uploads/images/2021/05/08/113689.jpg',
    description: 'Neymar da Silva Santos Júnior yang umumnya dikenal sebagai Neymar atau Neymar Jr, adalah pemain sepak bola profesional Brasil yang bermain untuk klub Prancis Paris Saint-Germain. Dia bermain sebagai penyerang atau pemain sayap.',
  ),
  Player(
    id: 'm4',
    categories: [
      'c4',
    ],
    title: 'Manuel Neuer',
    imageUrl:
        'https://awsimages.detik.net.id/community/media/visual/2022/03/28/manuel-neuer-4.jpeg?w=1200',
    description: 'Manuel Peter Neuer merupakan seorang pemain sepak bola Jerman, yang berposisi sebagai penjaga gawang. Saat ini ia bermain untuk Bayern München. Ia bermain untuk tim nasional Jerman.',
  ),
  Player(
    id: 'm5',
    categories: [
      'c4',
    ],
    title: 'Alisson Becker',
    imageUrl:
        'https://cdn.vox-cdn.com/thumbor/yP9oK3ybtFYkdHMjoPTemhETmmc=/0x229:1594x2964/1400x933/filters:focal(1015x435:1341x761):no_upscale()/cdn.vox-cdn.com/uploads/chorus_image/image/70192087/1355390030.0.jpg',
    description: 'Alisson Becker adalah seorang pemain sepak bola berkewarganegaraan Brasil yang bermain untuk klub Liverpool F.C. pada posisi Penjaga gawang. Alisson Becker bermain untuk klub Liverpool sejak tahun 2018, dan membela tim nasional Brasil sejak tahun 2015.',
  ),
];
